# Library Management System (C++)

This is a simple library management system implemented in C++. It allows for basic operations such as adding, modifying, and deleting both books and student records, as well as issuing and depositing books.

## Features
- **Administrator Menu**: Provides options to manage student records and books.
- **Book Issue and Deposit**: Allows issuing and depositing books for students.
- **Data Persistence**: Records are stored in files (`book.dat` and `student.dat`) for persistent storage.

## Getting Started

1. **Compilation**: Compile the `library_management_system.cpp` file using a C++ compiler.
    ```bash
    g++ library_management_system.cpp -o library_management_system
    ```

2. **Execution**: Run the compiled executable.
    ```bash
    ./library_management_system
    ```

## Usage
- Upon running the program, you will be presented with the main menu.
- Choose options according to your requirements:
  - **Book Issue**: Issue a book to a student.
  - **Book Deposit**: Return a book issued by a student.
  - **Administrator Menu**: Access administrative functions such as adding, modifying, or deleting records.
  - **Exit**: Quit the program.

## Administrator Menu
- **Create Student Record**: Add a new student record to the system.
- **Display All Students Record**: View all student records.
- **Display Specific Student Record**: Find and display details of a specific student.
- **Modify Student Record**: Modify details of an existing student record.
- **Delete Student Record**: Remove a student record from the system.
- **Create Book**: Add a new book to the library.
- **Display All Books**: View all books in the library.
- **Display Specific Book**: Find and display details of a specific book.
- **Modify Book**: Modify details of an existing book.
- **Delete Book**: Remove a book from the library.

## Note
- Ensure to input data accurately and follow the instructions provided by the system.
- For administrative tasks, use the Administrator Menu for easier management.

## Author
- Anushka Reddy

---

This README file provides an overview of the library management system and instructions for usage. If you encounter any issues or have suggestions for improvements, please feel free to reach out.
Enjoy managing your library efficiently!
